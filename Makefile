SHELL=/bin/bash

docker-host-run:
	docker-compose 	-f docker-compose.host-services.yaml pull
	docker-compose 	-f docker-compose.infrastructure.yaml \
					-f docker-compose.host-services.yaml \
					up -d

docker-host-down:
	docker-compose 	-f docker-compose.infrastructure.yaml -f docker-compose.host-services.yaml down

docker-host-ps:
	docker-compose 	-f docker-compose.infrastructure.yaml \
					-f docker-compose.host-services.yaml \
					ps




docker-run:
	docker-compose 	-f docker-compose.services.yaml pull
	docker-compose 	-f docker-compose.infrastructure.yaml \
					-f docker-compose.services.yaml \
					up -d

docker-down:
	docker-compose 	-f docker-compose.infrastructure.yaml -f docker-compose.services.yaml down

docker-ps:
	docker-compose 	-f docker-compose.infrastructure.yaml \
					-f docker-compose.services.yaml \
					ps